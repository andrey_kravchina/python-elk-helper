#!/usr/bin/env python3
# importing the requests library
import requests
import configparser
from elasticsearch import Elasticsearch

# index_name = "msc-fulllog-index-2019.12"
index_name = "common-service-exception-2019.12"
#index_name = "dp-common-exception-2019.12"
#index_name = "dp-site-recaptcha-2019.12"
#index_name = "dp-api-http-request-2019.12"
#index_name = "readservice-request-exception-index-2019.12"
#index_name = "dp-common-search-request-2019.12"
#index_name = "readservice-tracelog-index-2019.12"

index_name_SEARCH = index_name + '.*0000*'
index_name_REINDEX = index_name + '----reindex'

config = configparser.ConfigParser()
config.read('config.ini')

res = requests.get('http://'+config['Elasticsearch']['host']+':9200')
print(res.content)

es = Elasticsearch([{'host': config['Elasticsearch']['host'], 'port': config['Elasticsearch']['port']}]
                   ,timeout=3000, max_retries=10, retry_on_timeout=True)
target = es.indices.get(index_name_SEARCH)
for t in target:
    print(t)
print(len(target))
if len(target) > 0:
    result = es.reindex({
        "source": {"index": index_name_SEARCH},
        "dest": {"index": index_name_REINDEX}
    },timeout='1h', wait_for_completion=True)

    print(result)

    if result['total'] and result['took'] and not result['timed_out']:
        print("Seems reindex was successfull!")

        result = es.indices.put_settings('{"settings": {"index.blocks.write": true}}', index_name_REINDEX)
        print(result)

        result = es.indices.shrink(index_name_REINDEX, index_name, '''
        {
            "settings": {
            "index.number_of_replicas": 0,
            "index.number_of_shards": 1,
            "index.codec": "best_compression"
        }
        }
        ''',timeout='1h',wait_for_active_shards=1)

        print(result)

        result = es.indices.forcemerge(index_name,max_num_segments=1,flush=True)

        print(result)

        print("going to delete the old index!")

        es.indices.delete(index_name_REINDEX)
        es.indices.delete(index_name_SEARCH)
