from datetime import date, timedelta

today = date.today()
print(today)
first = today.replace(day=1)
print(first)
lastMonth = first - timedelta(days=1)
firstDayLastMonth = lastMonth.replace(day=1)
print(lastMonth.strftime("%Y-%m"))

last2Month = firstDayLastMonth - timedelta(days=1)
# print(last2Month.strftime("%Y-%m"))

last2Month_str = last2Month.strftime("%Y.%m")
print(last2Month_str)

firstDayLast2Month = last2Month.replace(day=1)
print(lastMonth.strftime("%Y-%m"))

last3Month = firstDayLast2Month - timedelta(days=1)
last3Month_str = last3Month.strftime("%Y.%m")
print(last3Month_str)

import requests
import configparser
from elasticsearch import Elasticsearch

for date_for_search in last3Month_str,last2Month_str:

    index_SEARCH_by_date = '*' + date_for_search + '.*0000*'

    config = configparser.ConfigParser()
    config.read('config.ini')

    es = Elasticsearch([{'host': config['Elasticsearch']['host'], 'port': config['Elasticsearch']['port']}]
                       ,timeout=3000, max_retries=10, retry_on_timeout=True)
    target = es.indices.get(index_SEARCH_by_date)
    for t in target:
        print(t)

        target_index = t.split(date_for_search)

        # for res_date in target_index:
        #     print(res_date)
        print(target_index[0])
        target_index_name = target_index[0]

        index_name = target_index_name + date_for_search

        index_name_SEARCH = index_name + '.*0000*'
        index_name_REINDEX = index_name + '----reindex'

        print(index_name)
        print(index_name_SEARCH)
        print(index_name_REINDEX)

        exit()